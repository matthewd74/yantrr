FROM resin/armv7hf-systemd:jessie 

ENV INITSYSTEM on

RUN apt-get update && apt-get install -y usbutils module-init-tools wvdial && echo "replacedefaultroute" >> /etc/ppp/options

ADD init.sh /init.sh

RUN chmod a+x /init.sh

ADD wvdial.conf /etc/wvdial.conf

CMD /init.sh
