#!/bin/bash


# The following is just for diagnostics and can be removed later on.
lsusb

until [ -c /dev/ttyUSB3 ]
do
      echo "Waiting for ttyUSB3.. Trying to power on the modem"
     
      # Incase the USB modem driver is preloaded
      rmmod usbserial || true 

      # The Modem can be powered on by providing a low level pulse at ON/OFF Pin for 3-4s when the module is off. 
      # It can be achieved by either using the push button switch S1 or GPIO45
      if [ ! -d /sys/class/gpio/gpio45  ]; then
	  echo 45 > export
      fi
      cd /sys/class/gpio/gpio45
      echo "high" > direction
      sleep 4
      echo "low" > direction
      sleep 4
      echo "high" > direction
      
      # Giving the modem a little time to power on before trying to modprobe
      sleep 5
      modprobe usbserial vendor=0x21f5 product=0x2012
      sleep 5

done

# The following is just for diagnostics and can be removed later on.
lsusb

wvdial

while :
do
	echo "Connected - Please check dashboard"
	sleep 60
done
